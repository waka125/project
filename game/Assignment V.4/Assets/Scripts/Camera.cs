﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

    public Transform MainPlayer;
    public Vector3 offset;

    void Update()
    {
        transform.position = new Vector3(MainPlayer.position.x + offset.x, MainPlayer.position.y + offset.y, offset.z); // Camera follows the player with specified offset position
    }
}
