﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {


    public void LoadLevel(string name)
    {
        Debug.Log("Level is being loaded: " + name);//Debug.Log is the same as print.
        SceneManager.LoadScene(name);
    }
}
