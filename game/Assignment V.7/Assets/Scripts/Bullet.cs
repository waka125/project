﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
    
    void OnCollisionEnter2D(Collision2D col)
    {

        if (col.gameObject.name == "Logs")
        {
            Destroy(col.gameObject);
            Destroy(gameObject);
        }

        else if (col.gameObject.name == "Frog")
        {
            Destroy(col.gameObject);
            Destroy(gameObject);
        }
        

    }

}
