﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Fire : MonoBehaviour
{


    public KeyCode shootKey = KeyCode.F;
 
    public Rigidbody2D projectile;
    public float projectileSpeed = 8f;


    

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            var fireballInst = Instantiate(projectile, transform.position, Quaternion.Euler(new Vector2(0, 0)));
            fireballInst.velocity = new Vector2(projectileSpeed, 0);
            Destroy(GameObject.Find("projectile(Clone)"), 0.2f);

        }

    }
}

