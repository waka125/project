﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frog : MonoBehaviour {
    public int maxHealth = 100;
    public int curHealth = 100;


    public Transform target;
    public int moveSpeed;
    public int rotationSpeed;

    private Transform myTransform;

    // Use this for initialization
    void Awake()
    {
        myTransform = transform;
    }

    void Start()
    {
        GameObject go = GameObject.FindGameObjectWithTag("Player");

        target = go.transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 dir = target.position - myTransform.position;
        dir.z = 0.0f; // Only needed if objects don't share 'z' value
        if (dir != Vector3.zero)
        {
            myTransform.rotation = Quaternion.Slerp(myTransform.rotation,
                                                     Quaternion.FromToRotation(Vector3.right, dir), rotationSpeed * Time.deltaTime);
        }

        //Move Towards Target
        myTransform.position += (target.position - myTransform.position).normalized * moveSpeed * Time.deltaTime;
    }


void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Bullet")
        {
            AddjustCurrentHealth(-10);
        }
    }

    public void AddjustCurrentHealth(int adj)
    {
        curHealth += adj;

        if (curHealth < 0)
            curHealth = 0;

        if (curHealth > maxHealth)
            curHealth = maxHealth;

        if (maxHealth < 1)
            maxHealth = 1;

        if (curHealth < 10)
            Destroy(gameObject);
    }
}
